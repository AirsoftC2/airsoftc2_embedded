import abc

class base_type(abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def inserter(self):
        pass

class position:
    def __init__(self, nodeId, time, lat, lon, alt):
        self.nodeId = nodeId
        self.time = time
        self.lat = lat
        self.lon = lon
        self.alt = alt

    def inserter(self):
        return "INSERT INTO position VALUES (?,?,?,?,?)", (self.nodeId, self.time, self.lat, self.lon, self.alt)

class packet_stats(base_type):
    def __init__(self, localId, remoteId, length, rssi, snr):
        self.localId = localId
        self.remoteId = remoteId
        self.length = length
        self.rssi = rssi
        self.snr = snr

    def inserter(self):
        return "INSERT INTO packet_stats VALUES (?,?,?,?,?)", (self.localId, self.remoteId, self.length, self.rssi, self.snr)
