import logging

def enable_logger(loggerArgs=None):
    if loggerArgs is None:
        logging.basicConfig(filename='radio.log',
                             format='[%(asctime)s %(levelname)s %(module)s::%(funcName)s]: %(message)s',
                             datefmt="%Y-%m-%d %H:%M:%S",
                             level=logging.DEBUG)

    else:
        logging.basicConfig(filename=loggerArgs['filename'],
                             format='[%(asctime)s %(levelname)s %(module)s::%(funcName)s]: %(message)s',
                             datefmt="%Y-%m-%d %H:%M:%S",
                             level=loggerArgs['level'])