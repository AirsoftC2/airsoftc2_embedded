#! /usr/bin/python3

import argparse
import logging
import time
import signal
from configparser import ConfigParser
from multiprocessing import Queue

from processors import database, gps_poller, transceiver
from util import config, logging_config

def start(procs):
    for proc in procs:
        proc.start()

def stop(procs):
    for proc in procs[::-1]:
        proc.stop()
        proc.join()

def main(args: argparse.ArgumentParser):
    logging_config.enable_logger()

    queue = Queue()

    cfg = ConfigParser()
    cfg.read(args.config)

    # check for run time, if run time is not set
    # then use an signal handler to whatch for sigint
    if cfg.has_option('params', 'run_time'):
        run_time = cfg['params'].getint('run_time')
        wait_func = time.sleep
        logging.info("Running for {run_time} seconds")
    else:
        signal.signal(signal.SIGINT, stop)
        run_time = None
        wait_func = lambda _: signal.pause()
        logging.info("Waiting for kill signal...")

    processors = []

    db_proc = database.db_thread(cfg, queue)
    processors.append(db_proc)

    gps = gps_poller.gps_thread(queue, fake_position=True)
    processors.append(gps)

    txrx = transceiver.txrx_thread(cfg, queue)
    processors.append(txrx)

    start(processors)

    wait_func(run_time)
    
    stop(processors)


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", "-C", required=False, type=str, default="default.cfg")

    _args = parser.parse_args()
    main(_args)
