import time
import spidev

class spi_driver:
    def __init__(self, spi_bus, spi_device, baudrate=1000000, write_mask=0x80):
        self._spi = spidev.SpiDev(spi_bus, spi_device)
        self._spi.max_speed_hz = baudrate
        self._write_mask = write_mask
        self._read_mask = ~self._write_mask

    def spi_read(self, reg):
        read_val = self._spi.xfer2([reg & self._read_mask, 0])
        return read_val[-1]

    def spi_write(self, reg, val):
        ret = self._spi.xfer2([reg | self._write_mask, val])
        return ret

    def spi_write_2(self, reg, vals):
        buffer = [reg | self._write_mask]
        buffer.extend(vals)
        return self._spi.xfer2(buffer)

if __name__ == "__main__":
    s = spi_driver(0, 0)
    print(s._spi.mode)
    s.spi_write(0x01, 0x00 | 0x80)
    time.sleep(0.1)
    print(s.spi_read(0x01))
