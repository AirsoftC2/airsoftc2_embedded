import logging
import time
from configparser import ConfigParser

import RPi.GPIO as GPIO

from .spi_driver import spi_driver
from .rh_95_utils import rf_95_registers, rf_95_values, modem_config

class rf_95_device(spi_driver):
    def __init__(self, config: ConfigParser):
        super().__init__(config.getint('spi_bus'), config.getint('spi_device'), config.getint('baudrate'), config.getint('write_mask'))
        self._int_pin = config.getint('interrupt_pin')
        self._rst_pin = config.getint('reset_pin')
        self._config = config
        self._callbacks = {}

        self._init_gpio()
        self._init_device()

    def _init_gpio(self):
        """Set up GPIO reset pin
        """
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self._int_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self._rst_pin, GPIO.OUT)
        GPIO.output(self._rst_pin, GPIO.HIGH)

        # # set up interrupt callback
        GPIO.add_event_detect(self._int_pin, GPIO.RISING, callback=self._handle_interrupt)

    def _init_device(self):
        """Initalize the board
        
        Raises:
            AttributeError -- [description]
        """

        # toggle the reset pin on the board
        GPIO.output(self._rst_pin, GPIO.LOW)
        time.sleep(0.01)
        GPIO.output(self._rst_pin, GPIO.HIGH)
        time.sleep(0.01)

        # set the operating mode
        self.mode = rf_95_values.RF95_LONG_RANGE_MODE
        time.sleep(0.01)
        if self.mode != rf_95_values.RF95_LONG_RANGE_MODE:
            raise AttributeError("Failed to set operating mode")

        # set center freq
        self.frequency = self._config.getfloat('frequency_hz')
        self.modem = modem_config.build_modem_config(self._config.getint('bandwidth_khz'),
                                                     self._config.getint('coding_rate'),
                                                     self._config.getint('spreading_factor'))

        self.preamble_length = 8
        self.tx_power = self._config.getint('tx_power_dBm')

    def cleanup(self):
        # toggle the reset pin on the board
        GPIO.output(self._rst_pin, GPIO.LOW)
        time.sleep(0.01)
        GPIO.output(self._rst_pin, GPIO.HIGH)
        time.sleep(0.01)

        # clean up GPIO
        GPIO.cleanup()

    def recv(self):
        pass

    def send(self, msg):
        logging.info("Sending message")

        # set mode to idle
        self.mode = rf_95_values.RF95_MODE_STDBY
        self.spi_write(rf_95_registers.RF95_REG_0D_FIFO_ADDR_PTR, 0)
        self.spi_write_2(rf_95_registers.RF95_REG_00_FIFO, msg)
        self.mode = rf_95_values.RF95_MODE_TX

    def register_interrupt_callback(self, key, callback):
        self._callbacks.update({key, callback})

    def _handle_interrupt(self, _):
        logging.info("Interrupt fired")

    @property
    def mode(self):
        mode = self.spi_read(rf_95_registers.RF95_REG_01_OP_MODE)
        return mode

    @mode.setter
    def mode(self, new_mode):
        self.spi_write(rf_95_registers.RF95_REG_01_OP_MODE, new_mode)

    @property
    def frequency(self):
        msb = self.spi_read(rf_95_registers.RF95_REG_06_FRF_MSB)
        mid = self.spi_read(rf_95_registers.RF95_REG_07_FRF_MID)
        lsb = self.spi_read(rf_95_registers.RF95_REG_08_FRF_LSB)

        return (msb << 16) | (mid << 8) | lsb

    @frequency.setter
    def frequency(self, frequency_hz):
        # map frequency
        freq = int((frequency_hz) // (32e6 // 2**19))

        logging.info("Setting center frequency to: {}".format(freq))

        # write the value
        self.spi_write(rf_95_registers.RF95_REG_06_FRF_MSB, (freq >> 16) & 0xff)
        self.spi_write(rf_95_registers.RF95_REG_07_FRF_MID, (freq >> 8) & 0xff)
        self.spi_write(rf_95_registers.RF95_REG_08_FRF_LSB, (freq) & 0xff)

    @property
    def tx_power(self):
        return self.spi_read(rf_95_registers.RF95_REG_09_PA_CONFIG)

    @tx_power.setter
    def tx_power(self, pwr):
        logging.info("Attempting to set the transmit power to: {} dBm".format(pwr))

        if pwr > 23:
            pwr = 23

        if pwr < 5:
            pwr = 5

        logging.info("Setting transmit power to: {} dBm".format(pwr))

        if pwr > 20:
            self.spi_write(rf_95_registers.RF95_REG_4D_PA_DAC, rf_95_values.RF95_PA_DAC_ENABLE)
            pwr -= 3

        else:
            self.spi_write(rf_95_registers.RF95_REG_4D_PA_DAC, rf_95_values.RF95_PA_DAC_DISABLE)


        self.spi_write(rf_95_registers.RF95_REG_09_PA_CONFIG, rf_95_values.RF95_PA_SELECT | (pwr - 5))

    def reset(self):
        """Reset the device
        """
        logging.info("Resetting device")
        self._init_device()

    @property
    def modem(self):
        bw_cr = self.spi_read(rf_95_registers.RF95_REG_1D_MODEM_CONFIG1)
        sf = self.spi_read(rf_95_registers.RF95_REG_1E_MODEM_CONFIG2)
        agc = self.spi_read(rf_95_registers.RF95_REG_26_MODEM_CONFIG3)

    @modem.setter
    def modem(self, new_settings):
        self.spi_write(rf_95_registers.RF95_REG_1D_MODEM_CONFIG1, new_settings.config_reg_1D)
        self.spi_write(rf_95_registers.RF95_REG_1E_MODEM_CONFIG2, new_settings.config_reg_1E)
        self.spi_write(rf_95_registers.RF95_REG_26_MODEM_CONFIG3, new_settings.config_reg_26)

    @property
    def preamble_length(self):
        pre_msb = self.spi_read(rf_95_registers.RF95_REG_20_PREAMBLE_MSB)
        pre_lsb = self.spi_read(rf_95_registers.RF95_REG_21_PREAMBLE_LSB)
        return (pre_msb << 8) | pre_lsb
    
    @preamble_length.setter
    def preamble_length(self, length):
        self.spi_write(rf_95_registers.RF95_REG_20_PREAMBLE_MSB, (length >> 8) & 0xff)
        self.spi_write(rf_95_registers.RF95_REG_21_PREAMBLE_LSB, length & 0xff)
        

if __name__ == "__main__":
    driver = rf_95_device(0, 0, 10, 12)