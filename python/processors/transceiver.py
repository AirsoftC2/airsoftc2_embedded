import logging
from multiprocessing import Process, Event, Queue
import time
import sqlite3
from configparser import ConfigParser

from util.proto import message_pb2

from rpi.rh_95_driver import rf_95_device

_QUERY_POSITION = "SELECT latitude, longitude, altitude FROM position ORDER BY time DESC LIMIT 1"

class txrx_thread(Process):
    def __init__(self, config: ConfigParser, queue: Queue, **kwargs):
        super(txrx_thread, self).__init__(**kwargs)

        self._device = rf_95_device(config['device'])
        self._queue = queue
        self.conn = sqlite3.connect(config["database"]['db_name'])

        self.running = Event()

    def stop(self):
        logging.info("Stopping transceiver thread")
        self.running.set()

    def run(self):
        logging.info("Starting transceiver thread")
        while not self.running.is_set():
            time.sleep(2)
            curr_pos = self.get_current_postion()

            if curr_pos is None:
                logging.warning("No GPS position")
                continue
            
            # serialize the message into bytes
            pos = message_pb2.position()
            pos.lat = curr_pos[0]
            pos.lon = curr_pos[1]
            pos.alt = curr_pos[2]
            msg = pos.SerializeToString()
            self._device.send(msg)
        self._device.cleanup()

    def _parse_received_msg(self):
        pass

    def _build_tx_rx_schedule(self):
        """TX RX slots are based on node ID
        """
        pass

    def get_current_postion(self):
        cur = self.conn.cursor()
        c = cur.execute(_QUERY_POSITION)
        pos = c.fetchone()
        return pos