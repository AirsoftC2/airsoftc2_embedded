import logging
from multiprocessing import Process, Event
import time

import gps

from util.types import position

class gps_thread(Process):
    def __init__(self, queue, fake_position=False, **kwargs):
        super(gps_thread, self).__init__(**kwargs)
        
        self._queue = queue
        self.running = Event()

        if fake_position:
            logging.warning("GPS POLLER IS FAKING POSITIONS")
            self.run = self._run_fake

        else:
            self.run = self._run_gpsd

    def stop(self):
        logging.info("Stopping gps poller")
        self.running.set()
    
    def _run_gpsd(self):
        logging.info("Starting gps poller")
        logging.info("Stopped gps poller")

    def _run_fake(self):
        logging.info("Starting gps poller")
        while not self.running.is_set():
            pos = position(0, time.time(), 38.8, -104.4, 2000)
            self._queue.put(pos)
            time.sleep(1)

        logging.info("Stopped gps poller")