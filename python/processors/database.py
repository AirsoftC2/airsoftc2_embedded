import logging
import sqlite3
from multiprocessing import Process, Event

class db_thread(Process):
    def __init__(self, config, queue, **kwargs):
        super(db_thread, self).__init__(**kwargs)
        self.conn = sqlite3.connect(config['database']['db_name'])
        self._init_tables()

        self.running = Event()
        self._queue = queue
        self._callbacks = {}

    def _init_tables(self):
        cur = self.conn.cursor()
        cur.execute("create table if not exists position (localId INTEGER, time long, latitude REAL, longitude REAL, altitude REAL)")
        cur.execute("create table if not exists packet_stats (localId INTEGER, remoteId INTEGER, length INTEGER, rssi REAL, snr REAL)")
        cur.execute("create table if not exists channels (localId INTEGER, remoteId INTEGER, frequency long, bandwidth INTEGER)")
        self.conn.commit()

    def stop(self):
        logging.info("Stopping database")
        self.running.set()
        self.conn.commit()
        self.conn.close()

    def _insert(self, msg):
        cur = self.conn.cursor()
        cur.execute(*msg.inserter())
        self.conn.commit()

    def run(self):
        logging.info("Starting database")

        while not self.running.is_set():
            if not self._queue.empty():
                logging.debug("Getting msg")
                msg = self._queue.get(block=False)
                self._insert(msg)

        logging.info("Stopped database")

    