# Airsoft Command and Control
Radio module control and database operations

# Radio Module
The radio module used can be found [here](https://learn.adafruit.com/adafruit-rfm69hcw-and-rfm96-rfm95-rfm98-lora-packet-padio-breakouts/rfm9x-test). The driver is written in python and draws heavily from [Adafruit's fork of AirSpayce's Radiohead library](https://github.com/adafruit/RadioHead)
